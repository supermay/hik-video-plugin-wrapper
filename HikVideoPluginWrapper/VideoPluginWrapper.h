
#ifndef _VIDEOPLUGINWRAPPER_H_
#define _VIDEOPLUGINWRAPPER_H_
#endif

#  define VIDEOPLUGIN_WRAPPER_EXPORT extern "C" __declspec(dllexport)

//错误码定义
#define VIDEOPLUGIN_ERR_FAIL                      -1       // 失败
#define VIDEOPLUGIN_ERR_SUCCESS                   0        // 成功
#define VIDEOPLUGIN_ERR_JSON_FORMAT               0x00100  // 非有效报文
#define VIDEOPLUGIN_ERR_INVALID_FUNCNAME          0x00101  // 功能标识无效
#define VIDEOPLUGIN_ERR_MISSING_REQUIRED_FIELD    0x00102  // 缺少必要字段
#define VIDEOPLUGIN_ERR_INVALID_FIELD_VALUE       0x00103  // 字段值无效
#define VIDEOPLUGIN_ERR_INVALID_FIELD_VALUE_TYPE  0x00104  // 字段值类型不匹配
#define VIDEOPLUGIN_ERR_INVALID_PLUGININSTANCE    0x00105  // 参数错误，插件实例无效
#define VIDEOPLUGIN_ERR_INVALID_TOOL_BAR_BUTTONIDS 0x00106 // 参数错误，toolBarButtonIDs字段值无效
#define VIDEOPLUGIN_ERR_INVALID_BUTTONIDS         0x00107  // 参数错误，buttonIDs字段值无效
#define VIDEOPLUGIN_ERR_INVALID_WNDID             0x00108  // 参数错误，wndId字段值无效
#define VIDEOPLUGIN_ERR_GETTED_RSA_KEY            0x00109  // 错误操作，重复获取公钥
#define VIDEOPLUGIN_ERR_INVALID_AUTHUUID          0x0010a  // 参数错误，authUuid字段值无效
#define VIDEOPLUGIN_ERR_REPEAT_AUTHUUID           0x0010b  // 参数错误，重复的authUuid字段，参数中authUuid重复或与插件中已存在的authUuid重复
#define VIDEOPLUGIN_ERR_UNINITIAL                 0x00150  // 未初始化
#define VIDEOPLUGIN_ERR_FUNCTION_NOT_SUPPORT      0x00200  // 功能不支持
#define VIDEOPLUGIN_ERR_SUPPORT_EZVIZ_DIRECT      0x00201  // 不支持直连萤石云
#define VIDEOPLUGIN_ERR_SUPPORT_LAYOUT            0x00202  // 布局不支持
#define VIDEOPLUGIN_ERR_INVALID_SNAP              0x00300  // 抓图失败
#define VIDEOPLUGIN_ERR_TALK_EXIST                0x00500  // 对讲占用中

typedef long long WRAPPER_INT64;
// 消息回调
typedef void(_stdcall *Wrapper_MessageCallback)(DWORD PlayerId, const char* pRspJsonMsg, DWORD dwUser);

VIDEOPLUGIN_WRAPPER_EXPORT BOOL Wrapper_ShowDialog(WRAPPER_INT64 parent);

// 创建实例
// 入参：
//     PlayerId：播放窗口标识
// 返回值： PlayerId：播放窗口标识，不成功返回VIDEOPLUGIN_ERR_FAIL
VIDEOPLUGIN_WRAPPER_EXPORT int Wrapper_CreatePluginInstance(DWORD PlayerId, const char* title);

// 销毁实例
// 入参：
//     PlayerId：播放窗口标识
// 返回值：BOOL
VIDEOPLUGIN_WRAPPER_EXPORT BOOL Wrapper_DestroyPluginInstance(DWORD PlayerId);

// 获取窗口句柄
// 入参：
//     PlayerId：播放窗口标识
// 返回值：非NULL为成功 NULL为失败 
//VIDEOPLUGIN_WRAPPER_EXPORT void * Wrapper_GetWindowHandle(DWORD PlayerId);

// 通用请求接口
// 入参：实例指针
//      PlayerId：播放窗口标识
//      pszJsonRequest：json格式请求报文，支持的功能以及使用方式详见“通用请求接口功能支持”
// 返回值：json格式报文（详见视频客户端插件开发指南文档），注意需要调Wrapper_ReleaseMemory释放内存
VIDEOPLUGIN_WRAPPER_EXPORT char* Wrapper_GerneralRequest(DWORD PlayerId, const char* pszJsonRequest);

// 用于释放Wrapper_GerneralRequest内存接口
// 入参：字符串数组指针
// 返回值：无
VIDEOPLUGIN_WRAPPER_EXPORT void Wrapper_ReleaseMemory(char* pszCharArray);

// 用于隐藏播放窗体接口
// 入参：PlayerId：播放窗口标识
// 返回值：无
VIDEOPLUGIN_WRAPPER_EXPORT void Wrapper_HideWin(DWORD PlayerId);

// 消息回调设置接口
// 入参：实例指针
//      PlayerId：播放窗口标识
//      fnMsgCB：回调函数地址
//      pUserData：用户数据
// 返回值：0-成功 -1-失败
VIDEOPLUGIN_WRAPPER_EXPORT DWORD Wrapper_SetMessageCallback(DWORD PlayerId, Wrapper_MessageCallback pfnMsgCB, DWORD dwUser);

