#pragma once
#include <QtWidgets/QMainWindow>
#include <string>
#include <QCheckBox>
#include "ui_VideoPluginWin.h"

class VideoPluginWin : public QMainWindow
{
	Q_OBJECT

public:
	VideoPluginWin(QWidget *parent = Q_NULLPTR);
	~VideoPluginWin();
private:
	void _initUi();

	void resizeEvent(QResizeEvent * event);
	void closeEvent(QCloseEvent * event);
	void resizePlugin();
	static void _stdcall cb_MessageCallback(void* pPluginInstance, const char* pRspJsonMsg, void* pUserData);
	bool nativeEvent(const QByteArray & eventType, void * message, long * result);
private:
	Ui::VideoWinClass ui;
	bool isTop = true;
public:
	void* _pluginInstance = nullptr;
	ulong PlayerId = -1;
	ulong CallbackUserData = -1;
	void* MessageCallback = nullptr;
};