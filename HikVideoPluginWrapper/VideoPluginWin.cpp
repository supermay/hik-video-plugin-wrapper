#include <QDesktopWidget>    //注意一定要这个头文件
#include <Windows.h>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QHBoxLayout>
#include "VideoPluginWin.h"
#include "VideoPlugin.h"
#include "VideoPluginWrapper.h"

#define IDM_STAYSONTOP 0x0010 
#define IDM_STAYSONTOP_TEXT "置顶"


VideoPluginWin::VideoPluginWin(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	//注意一定要在这个setupUi的后面
	//写上下面的四句，最后两句选中其一即可
	int x = (QApplication::desktop()->width() - this->width()) / 2;
	int y = (QApplication::desktop()->height() - this->height()) / 2;
	this->setGeometry(x, y, this->width(), this->height());

	_initUi();
}

VideoPluginWin::~VideoPluginWin()
{
	if (_pluginInstance != nullptr)
	{
		VideoPlugin_DestroyPluginInstance(_pluginInstance);
		_pluginInstance = nullptr;
	}
}

void VideoPluginWin::_initUi()
{
	//增加一个系统菜单
	HMENU hMenu = GetSystemMenu((HWND)this->winId(), false);
	if (hMenu)
	{
		AppendMenu(hMenu, MF_SEPARATOR, 0, NULL);
		AppendMenu(hMenu, MF_CHECKED | MF_STRING, IDM_STAYSONTOP, TEXT(IDM_STAYSONTOP_TEXT));
	}

	_pluginInstance = VideoPlugin_CreatePluginInstance((HWND)(ui.player->winId()));
	resizePlugin();
}

void VideoPluginWin::resizeEvent(QResizeEvent* event)
{
	QMainWindow::resizeEvent(event);
	resizePlugin();
}

void VideoPluginWin::closeEvent(QCloseEvent *event) 
{
	QString stopAllPreviewJson =  "{\"funcName\": \"stopAllPreview\"}";
	QString stopAllPlaybackJson = "{\"funcName\": \"stopAllPlayback\"}";
	
	//停止全部播放
	if (_pluginInstance != nullptr) {
		char*  ch;
		try
		{
			QByteArray ba = stopAllPreviewJson.toLatin1();
			ch = ba.data();
			char* pRet=VideoPlguin_GerneralRequest(_pluginInstance,ch);
			if (pRet != nullptr) {
				VideoPlugin_ReleaseMemory(pRet);
				pRet = nullptr;
			}
			delete ch;

			ba = stopAllPlaybackJson.toLatin1();
			ch = ba.data();
			pRet = VideoPlguin_GerneralRequest(_pluginInstance, ch);
			if (pRet != nullptr) {
				VideoPlugin_ReleaseMemory(pRet);
				pRet = nullptr;
			}
			delete ch;
		}		
		catch (QString exception) {
			
		}		
	}
	QMainWindow::closeEvent(event);
}

void VideoPluginWin::resizePlugin()
{
	if (_pluginInstance != nullptr)
	{
		QWidget* pluginWidget = QWidget::find((WId)VideoPlugin_GetWindowHandle(_pluginInstance));

		//修改widget->resize(300, 300)【注意其中大小就是你窗口的大小】
		pluginWidget->resize(this->size());

		/*ui.verticalLayout_8->addWidget(pluginWidget);*/
		VideoPlugin_SetMessageCallback(_pluginInstance, &VideoPluginWin::cb_MessageCallback, this);
	}
}

void VideoPluginWin::cb_MessageCallback(void * pPluginInstance, const char * pRspJsonMsg, void * pUserData)
{
	if (nullptr == pUserData)
	{
		return;
	}

	VideoPluginWin* pThis = reinterpret_cast<VideoPluginWin*>(pUserData);
	do
	{
		QJsonParseError jsonError;
		QJsonDocument document = QJsonDocument::fromJson(pRspJsonMsg, &jsonError);
		if (jsonError.error != QJsonParseError::NoError)
		{
			break;
		}
		if (!document.isObject())
		{
			break;
		}

		QJsonObject& obj = document.object();
		if (!obj.contains("type") || !obj.contains("msg"))
		{
			break;
		}

		int type = obj["type"].toInt();
		if (5 == type)// 进入全屏 / 退出全屏消息
		{
			QJsonObject& objEx = obj["msg"].toObject();
			if (objEx.contains("result"))
			{
				int result = objEx["result"].toInt();
				// 0x0400-全屏 0x0401-退出全屏
				if (result == 0x0400) {
					pThis->setWindowFlags(Qt::WindowCloseButtonHint | Qt::WindowMaximizeButtonHint);
				}
				else
				{
					pThis->setWindowFlags(Qt::WindowCloseButtonHint | Qt::WindowMaximizeButtonHint | Qt::WindowStaysOnTopHint);
					pThis->show();
				}
			}
		}

	} while (false);

	if (nullptr != pThis->MessageCallback) {
		try
		{
			Wrapper_MessageCallback callbck = (Wrapper_MessageCallback)pThis->MessageCallback;
			callbck(pThis->PlayerId, pRspJsonMsg, pThis->CallbackUserData);
		}
		catch (const std::exception&)
		{
			//
		}		
	}
}


bool VideoPluginWin::nativeEvent(const QByteArray &eventType, void *message, long *result)
{
	MSG* msg = (MSG*)message;
	switch (msg->message)
	{
	case WM_SYSCOMMAND:
		if ((msg->wParam & 0xfff0) == IDM_STAYSONTOP) {

			if(this->isTop){
				::SetWindowPos((HWND)this->winId(), HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
			}
			else {
				::SetWindowPos((HWND)this->winId(), HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
			}

			
			HMENU hMenu = GetSystemMenu((HWND)this->winId(), false);
			if (hMenu)
			{
				if (this->isTop) {
					ModifyMenu(hMenu, IDM_STAYSONTOP, MF_UNCHECKED | MF_STRING, IDM_STAYSONTOP, TEXT(IDM_STAYSONTOP_TEXT));
				}
				else {
					ModifyMenu(hMenu, IDM_STAYSONTOP, MF_CHECKED | MF_STRING, IDM_STAYSONTOP, TEXT(IDM_STAYSONTOP_TEXT));
				}
								
			}
			this->isTop = !this->isTop;

			return true;
		}		
	}

	return false;
}