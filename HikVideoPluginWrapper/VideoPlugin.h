#ifndef _HIK_VIDEOPLUGIN_H_
    #define _HIK_VIDEOPLUGIN_H_
#endif

# if defined(VIDEOPLUGIN_LIB)
#  define VIDEOPLUGIN_EXPORT extern "C" __declspec(dllexport)
# else
#  define VIDEOPLUGIN_EXPORT extern "C" __declspec(dllimport)
# endif

#ifndef CALLBACK
    #define CALLBACK __stdcall
#endif  // CALLBACK

#ifndef _IN_
    #define _IN_    // 表示入参
#endif

//错误码定义
#define VIDEOPLUGIN_ERR_FAIL                      -1       // 失败
#define VIDEOPLUGIN_ERR_SUCCESS                   0        // 成功
#define VIDEOPLUGIN_ERR_JSON_FORMAT               0x00100  // 非有效报文
#define VIDEOPLUGIN_ERR_INVALID_FUNCNAME          0x00101  // 功能标识无效
#define VIDEOPLUGIN_ERR_MISSING_REQUIRED_FIELD    0x00102  // 缺少必要字段
#define VIDEOPLUGIN_ERR_INVALID_FIELD_VALUE       0x00103  // 字段值无效
#define VIDEOPLUGIN_ERR_INVALID_FIELD_VALUE_TYPE  0x00104  // 字段值类型不匹配
#define VIDEOPLUGIN_ERR_INVALID_PLUGININSTANCE    0x00105  // 参数错误，插件实例无效
#define VIDEOPLUGIN_ERR_INVALID_TOOL_BAR_BUTTONIDS 0x00106 // 参数错误，toolBarButtonIDs字段值无效
#define VIDEOPLUGIN_ERR_INVALID_BUTTONIDS         0x00107  // 参数错误，buttonIDs字段值无效
#define VIDEOPLUGIN_ERR_INVALID_WNDID             0x00108  // 参数错误，wndId字段值无效
#define VIDEOPLUGIN_ERR_GETTED_RSA_KEY            0x00109  // 错误操作，重复获取公钥
#define VIDEOPLUGIN_ERR_INVALID_AUTHUUID          0x0010a  // 参数错误，authUuid字段值无效
#define VIDEOPLUGIN_ERR_REPEAT_AUTHUUID           0x0010b  // 参数错误，重复的authUuid字段，参数中authUuid重复或与插件中已存在的authUuid重复
#define VIDEOPLUGIN_ERR_UNINITIAL                 0x00150  // 未初始化
#define VIDEOPLUGIN_ERR_FUNCTION_NOT_SUPPORT      0x00200  // 功能不支持
#define VIDEOPLUGIN_ERR_SUPPORT_EZVIZ_DIRECT      0x00201  // 不支持直连萤石云
#define VIDEOPLUGIN_ERR_SUPPORT_LAYOUT            0x00202  // 布局不支持
#define VIDEOPLUGIN_ERR_INVALID_SNAP              0x00300  // 抓图失败
#define VIDEOPLUGIN_ERR_TALK_EXIST                0x00500  // 对讲占用中


// 消息回调
typedef void(_stdcall *VideoPlugin_MessageCallback)(void* pPluginInstance, const char* pRspJsonMsg, void* pUserData);

// 创建实例
// 入参：
//     pParentWndHandle：父窗口句柄
// 返回值：实例指针
VIDEOPLUGIN_EXPORT void* VideoPlugin_CreatePluginInstance(_IN_ void* pParentWndHandle);

// 销毁实例
// 入参：
//     pPluginInstance：实例指针
// 返回值：无
VIDEOPLUGIN_EXPORT void VideoPlugin_DestroyPluginInstance(_IN_ void* pPluginInstance);

// 获取窗口句柄
// 入参：
//     pPluginInstance：实例指针
// 返回值：非NULL为成功 NULL为失败 
VIDEOPLUGIN_EXPORT void* VideoPlugin_GetWindowHandle(_IN_ void* pPluginInstance);

// 通用请求接口
// 入参：实例指针
//      pPluginInstance：实例指针
//      pszJsonRequest：json格式请求报文，支持的功能以及使用方式详见“通用请求接口功能支持”
// 返回值：json格式报文（详见视频客户端插件开发指南文档），注意需要调VideoPlugin_ReleaseMemory释放内存
VIDEOPLUGIN_EXPORT char* VideoPlguin_GerneralRequest(_IN_ void* pPluginInstance, _IN_ const char* pszJsonRequest);

// 用于释放VideoPlguin_GerneralRequest内存接口
// 入参：字符串数组指针
// 返回值：无
VIDEOPLUGIN_EXPORT void VideoPlugin_ReleaseMemory(_IN_ char* pszCharArray);

// 消息回调设置接口
// 入参：实例指针
//      pPluginInstance：实例指针
//      fnMsgCB：回调函数地址
//      pUserData：用户数据
// 返回值：0-成功 -1-失败
VIDEOPLUGIN_EXPORT int VideoPlugin_SetMessageCallback(_IN_ void* pPluginInstance, _IN_ VideoPlugin_MessageCallback pfnMsgCB, _IN_ void* pUserData);
