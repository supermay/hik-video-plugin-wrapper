
#include <qmfcapp.h>
#include <qwinwidget.h>
#include <qmap.h>
#include <QMessageBox>
#include <windows.h>
#include "VideoPluginWrapper.h"
#include "VideoPluginWin.h"
#include "VideoPlugin.h"

static QMap<DWORD, VideoPluginWin*> WrapperWinMap;

void UpdateWrapperMap(DWORD PlayerId, VideoPluginWin*  wInfo) {
	if (WrapperWinMap.contains(PlayerId)) //判断map里是否已经包含某“键-值”
	{
		WrapperWinMap[PlayerId] = wInfo;
	}
	else
	{
		WrapperWinMap.insert(PlayerId, wInfo);
	}
}

void ShowWin(VideoPluginWin* pWin, bool isShow = true) {
	//显示窗体
	if (pWin != nullptr) {
		if (isShow) {
			pWin->setWindowFlags( Qt::WindowCloseButtonHint | Qt::WindowMaximizeButtonHint | Qt::WindowStaysOnTopHint);
			pWin->show();
		}
		else {
			pWin->hide();
		}
	}
}

VideoPluginWin* GetWinByPlayId(DWORD PlayerId) {
	VideoPluginWin* wInfo = nullptr;

	if (WrapperWinMap.contains(PlayerId)) {
		wInfo = WrapperWinMap[PlayerId];
	}
	else
	{
		wInfo = new VideoPluginWin();
		wInfo->PlayerId = PlayerId;
		ShowWin(wInfo,false);
	}

	UpdateWrapperMap(PlayerId, wInfo);
	
	return wInfo;
}

BOOL DeleteWrapperMap(DWORD PlayerId) {
	if (WrapperWinMap.contains(PlayerId)) //判断map里是否已经包含某“键-值”
	{
		VideoPluginWin* wInfo = WrapperWinMap[PlayerId];
		if (wInfo != nullptr) {
			wInfo->setAttribute(Qt::WA_QuitOnClose,true);
		}
		delete wInfo;
		//直接根据key值删除
		WrapperWinMap.remove(PlayerId);

		return TRUE;
	}
	return FALSE;
}

BOOL WINAPI DllMain( HINSTANCE hInstance, DWORD dwReason, LPVOID /*lpvReserved*/ )
{
    static bool ownApplication = FALSE;
	

	if (dwReason == DLL_PROCESS_ATTACH) {
		ownApplication = QMfcApp::pluginInstance(hInstance);
	}
	if (dwReason == DLL_PROCESS_DETACH && ownApplication) {
		qApp->quit();
		delete qApp;
	}

    return TRUE;
}

BOOL Wrapper_ShowDialog(WRAPPER_INT64 parent)
{
    QWinWidget win( (HWND)parent );
    win.showCentered();
    QMessageBox::about( &win, "About VideoPluginWrapper", "VideoPluginWrapper Version 1.0\nCopyright (C) 2021" );

    return TRUE;
}

int Wrapper_CreatePluginInstance(DWORD PlayerId, const char* title)
{
	QString s = QString::fromLocal8Bit(title);

	VideoPluginWin*  pWrapperWin = GetWinByPlayId(PlayerId);

	if (pWrapperWin != nullptr) {
		pWrapperWin->setWindowTitle(s);

		return PlayerId;
	}
	return -1;
}

BOOL Wrapper_DestroyPluginInstance(DWORD PlayerId)
{
	return DeleteWrapperMap(PlayerId);
}

//void * Wrapper_GetWindowHandle(DWORD PlayerId)
//{
//	VideoPluginWin*  pWrapperWin = GetWinByPlayId(PlayerId);
//	if (pWrapperWin != nullptr) {
//		return VideoPlugin_GetWindowHandle(pWrapperWin->_pluginInstance);
//	}
//	return nullptr;	
//}

char* Wrapper_GerneralRequest(DWORD PlayerId, const char* pszJsonRequest)
{
	VideoPluginWin*  pWrapperWin = GetWinByPlayId(PlayerId);
	ShowWin(pWrapperWin);

	if (pWrapperWin != nullptr) {
		return VideoPlguin_GerneralRequest(pWrapperWin->_pluginInstance, pszJsonRequest);
	}
	return nullptr;
}

void Wrapper_ReleaseMemory(char* pszCharArray)
{
	VideoPlugin_ReleaseMemory(pszCharArray);
}

void Wrapper_HideWin(DWORD PlayerId)
{
	VideoPluginWin*  pWrapperWin = GetWinByPlayId(PlayerId);
	ShowWin(pWrapperWin,false);
	return;
}

DWORD Wrapper_SetMessageCallback(DWORD PlayerId, Wrapper_MessageCallback pfnMsgCB, DWORD dwUser)
{
	VideoPluginWin*  pWrapperWin = GetWinByPlayId(PlayerId);

	if (pWrapperWin != nullptr) {
		if (pfnMsgCB != nullptr) {
			bool bHidden = pWrapperWin->isHidden();
			pWrapperWin->CallbackUserData = dwUser;
			pWrapperWin->MessageCallback = pfnMsgCB;
			if (bHidden) {
				pWrapperWin->hide();
			}
		}
		return PlayerId;
		//return VideoPlugin_SetMessageCallback(pWrapperWin->_pluginInstance, pfnMsgCB, pUserData);
	}
	return -1;	
}
