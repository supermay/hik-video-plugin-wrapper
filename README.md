# HikVideoPluginWrapper

#### 介绍
海康威视综合安防平台iSecure Center V1.5.100的客户端SDK（VideoPlugin）基于Visual Studio2015（简称VS2015）和Qt5.9.6开发，
不能被DELPHI等调用，因而增加一个Wrapper以便使用，只是简单地把VideoPlugin.dll再封装一次，Wrapper_GerneralRequest中使用到的JSON数据
与VideoPlguin_GerneralRequest中的一样，详参 视频客户端插件 V1.5.0开发指南.pdf。因为QT的消息循环与Window的不同，
所以取消VideoPlugin_GetWindowHandle的封装。 本工程使用qtwinmigrate把QT的界面导出为delphi使用。下载地址：https://github.com/sorcererq/qtwinmigrate
第一次使用QT，感觉还可以，当然也有不少值得改进的地方，请多多指教。


#### 软件架构
软件架构说明


#### 开发环境

1.  Visual Studio2015
2.  Qt5.9.6 + msvs2015 x86编译

#### 使用说明

1.  生成文件HikVideoPluginWrapper.dll
2.  函数导出VideoPluginWrapper.h
3.  要包含VideoPlugin.dll

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

#### 您也可以打赏
![image](01.jpg)
![image](02.jpg)
